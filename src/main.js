const express = require('express')
const { reporters } = require('mocha')
const { letters } = require('./mylib')
const app = express()
const port = 3000
const mylib = require('./mylib')
// endpoint localhost:3000/
app.get('/', (req, res) => {
    res.send('We need random numbers.')
})
// endpoint localhost:3000/add
app.get('/add', (req, res) => {
    const newNumber = mylib.random().toString()
    const newNumber2 = mylib.random().toString()
    res.send('Random numbers here: ' + newNumber + ' and ' + newNumber2)
})
    // endpoint http://localhost:3000/add/more?a=1&b=1

    app.get('/add/more', (req, res) => {
    const a = parseInt(req.query.a)
    const b = parseInt(req.query.b)
    console.log({a , b}) //aaltosulut näyttää myös muuttujan
    const total = mylib.sum(a,b).toString()
    const word = "cake"
    res.send('Sum is ' + total.toString() + '. BTW Cake has ' + mylib.letters(word)) + ' letters.'
    })


app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})