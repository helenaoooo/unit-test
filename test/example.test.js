

const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib')

describe('Unit testing mylib.js', () => {

    
    before(() => {
        myvar = 1;
        console.log("test starting")
        
        
    })
    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1, 1)
        expect(result).to.equal(2)
    })
    it('Assert numbers are not the same.', () => {
        const newNumber = mylib.random().toString()
        const newNumber2 = mylib.random().toString()
        assert(newNumber !== newNumber2)
    })
    it.skip('Myvar should exist', () => {
        should.exist(myvar)
    })
    it('If word is "cake", it has 4 letters.', () => {
        expect(mylib.letters("cake")).to.equal(4)

    })
    after(() => {
        console.log('Testing over.')
    })
})